package com.diplom.math

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Daniel Mironov on 17.05.2019
 */

class RungeSolverTest {
    private val rungeSolver: RungeSolver = RungeSolver()

    @Test
    fun solveDiffure_linearEq() {
        val y0 = 1.0
        val list0 = ArrayList<Double>(1)
        list0.add(y0)
        val listResult = rungeSolver
            .solve({ input: List<Double> -> linearFunc(input) }, list0, 1.0)
        val result = listResult[0]
        assertEquals(Math.E, result, 0.1)
    }

    private fun linearFunc(y: List<Double>): List<Double> {
        if (y.size != 1) {
            throw IllegalArgumentException("Wrong test input!")
        }
        val result = y[0]
        return listOf(result)
    }
}