package com.diplom.math

import com.diplom.assertDoubleListEquals
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Test
import kotlin.math.pow

class GradientCalculatorTest {

    private val calculator: GradientCalculator = GradientCalculator()

    @Test
    fun test() {
        val result = calculator.calculate(
            this::testFun,
            listOf(1.0, 1.0, 1.0)
        )

        val expectedResult = listOf(4.0, 4.0, 2.0)

        assertDoubleListEquals(expectedResult, result)
    }

    @Test
    fun testInZero () {
        val result = calculator.calculate(
            {
                it[0].pow(2)
            },
            listOf(0.0)
        )

        val expectedResult = listOf(0.0)

        assertDoubleListEquals(expectedResult, result)
    }

    private fun testFun(input: List<Double>): Double {
        return input[0].pow(2.0) + 2.0 * (input[0] * input[1]) + input[1].pow(2.0) + input[2].pow(2.0)
    }
}