package com.diplom.math

import com.diplom.assertDoubleListEquals
import org.junit.Test
import kotlin.math.pow

class MultiCoordinationMinFinderTest {

    @Test
    fun test() {
        val finder = MultiCoordinationMinFinder(
            listOf(10.0, 10.0),
            listOf(Pair(-100.0, 100.0), Pair(-100.0, 100.0)),
            this::rosenbrock,
            0.0
        )

        val result = finder.findPoints()
        val expected = listOf(1.0, 1.0)

        assertDoubleListEquals(expected, result)
    }

    private fun rosenbrock(input: List<Double>): Double {
        val x = input[0]
        val y = input[1]

        return (1.0 - x).pow(2) + 100.0 * ((y - x.pow(2)).pow(2))
    }
}