package com.diplom

import org.junit.Assert

fun assertDoubleListEquals (expectedResult : List<Double>, result: List<Double>) {
    expectedResult.indices.forEach {
        Assert.assertEquals(expectedResult[it], result[it], 0.1)
        println(result[it])
    }
}