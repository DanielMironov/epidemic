package com.diplom.smo

import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Created by Daniel Mironov on 20.05.2019
 */

class RequestFactoryTest {
    private val factory = RequestFactory(30, 1.0, 4, 30)

    @Test
    fun testRequestTime() {
        assertTrue(factory.isTimeForRequest(0))
        assertTrue(factory.isTimeForRequest(15))
        assertTrue(factory.isTimeForRequest(30))
        assertTrue(factory.isTimeForRequest(45))
    }
}