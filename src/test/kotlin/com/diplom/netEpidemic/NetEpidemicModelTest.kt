package com.diplom.netEpidemic

import com.nhaarman.mockitokotlin2.*
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Daniel Mironov on 30.04.2019
 */

class NetEpidemicModelTest {
    private val model = NetEpidemicModel()

//    @Test
//    fun testStressNetCreating() {
//        val healthyCount = 5000
//        val sickCount = 3000
//        val immuneCount = 2000
//        val halfConnectedAmount = 45
//        model.add(getMockedPeople(healthyCount, sickCount, immuneCount))
//        //model.createNet(halfConnectedAmount)
//        println("Net created!")
//        assertEquals(healthyCount, model.getHealthyCount())
//        assertEquals(sickCount, model.getSickCount())
//        assertEquals(immuneCount, model.getImmuneCount())
//    }

    @Test
    fun testCanCreateNet() {
        val netSize = 10
        val halfConnectedAmount = 2
        model.add(getMockedPeople(netSize, 0, 0))
        model.createNet(halfConnectedAmount)
        for (person in model.people) {
            verify(person, times(halfConnectedAmount)).addConnection(any())
        }
    }

    @Test
    fun testMeasurements() {
        testGetMeasurements()
        testMeasurementsAreCorrect()
    }

    private fun testGetMeasurements() {
        val healthyCount = model.getHealthyCount()
        val sickCount = model.getSickCount()
        val immuneCount = model.getImmuneCount()
        assertEquals(0, healthyCount)
        assertEquals(0, sickCount)
        assertEquals(0, immuneCount)
    }

    private fun testMeasurementsAreCorrect() {
        model.add(getMockedPeople(10, 5, 7))
        val healthyCount = model.getHealthyCount()
        val sickCount = model.getSickCount()
        val immuneCount = model.getImmuneCount()
        assertEquals(10, healthyCount)
        assertEquals(5, sickCount)
        assertEquals(7, immuneCount)
    }

    @Test
    fun testProgressing() {
        testStatsAreCorrectAfterProgressing()
        testStatsAreCorrectAfterProgressingTwice()
    }

    private fun testStatsAreCorrectAfterProgressing() {
        model.add(getMockedPeople(10, 5, 7))
        model.progress()
        val healthyCount = model.getStatsByHealthState(HealthState.HEALTHY).last()
        val sickCount = model.getStatsByHealthState(HealthState.SICK).last()
        val immuneCount = model.getStatsByHealthState(HealthState.IMMUNE).last()
        assertEquals(10, healthyCount)
        assertEquals(5, sickCount)
        assertEquals(7, immuneCount)
    }

    private fun testStatsAreCorrectAfterProgressingTwice() {
        model.add(getMockedPeople(10, 5, 7))
        model.progress()
        model.progress()
        val healthyCount = model.getStatsByHealthState(HealthState.HEALTHY).last()
        val sickCount = model.getStatsByHealthState(HealthState.SICK).last()
        val immuneCount = model.getStatsByHealthState(HealthState.IMMUNE).last()
        assertEquals(10, healthyCount)
        assertEquals(5, sickCount)
        assertEquals(7, immuneCount)
    }

    private fun getMockedPeople(healthyCount: Int, sickCount: Int, immuneCount: Int): Set<Person> {
        val preResult = emptySet<Person>().toMutableSet()
        var i = healthyCount
        while (i > 0) {
            preResult.add(getMockedPerson(HealthState.HEALTHY))
            i--
        }
        i = sickCount
        while (i > 0) {
            preResult.add(getMockedPerson(HealthState.SICK))
            i--
        }
        i = immuneCount
        while (i > 0) {
            preResult.add(getMockedPerson(HealthState.IMMUNE))
            i--
        }
        return preResult.toSet()
    }

    private fun getMockedPerson(reqHealthState: HealthState): Person {
        return mock {
            on { healthState } doReturn reqHealthState
        }
    }
}