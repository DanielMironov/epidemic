package com.diplom.netEpidemic

import com.diplom.netEpidemic.HealthState.*
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Daniel Mironov on 27.04.2019
 */

class PersonTest {

    private val person = Person()

    @Test
    fun testConnectionAddsBothWay() {
        val newPerson = Person()
        person.addConnection(newPerson)
        val expectedConnections = setOf(newPerson)
        val expectedNewConnections = setOf(person)
        val gotConnections = person.connections
        val gotNewConnections = newPerson.connections
        assertEquals(expectedConnections, gotConnections)
        assertEquals(expectedNewConnections, gotNewConnections)
    }

    @Test
    fun testNewPersonIsHealthy() {
        val healthState = person.healthState
        assertEquals(HEALTHY, healthState)
    }

    @Test
    fun testPersonCanGetSick() {
        person.getSick()
        val healthState = person.healthState
        assertEquals(SICK, healthState)
    }

    @Test(expected = IllegalStateException::class)
    fun testPersonCantGetImmuneIfHeIsHealthy() {
        person.getWell()
    }

    @Test
    fun testPersonCanGetImmune() {
        person.getSick()
        person.getWell()
        val healthState = person.healthState
        assertEquals(IMMUNE, healthState)
    }
}