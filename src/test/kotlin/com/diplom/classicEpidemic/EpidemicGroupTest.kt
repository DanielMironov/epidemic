package com.diplom.classicEpidemic

import org.junit.Assert.assertEquals
import org.junit.Ignore
import org.junit.Test

/**
 * Created by Daniel Mironov on 17.05.2019
 */

const val TEST_START_HEALTHY = 700.0
const val TEST_START_SICK = 200.0
const val TEST_START_IMMUNE = 70.0
const val TEST_START_VACCINATED = 30.0
const val TEST_START_DEAD = 0.0

const val TEST_SICKEN = 0.1
const val TEST_VACCINATE = 0.05
const val TEST_REHAB = 0.2
const val TEST_DEATH = 0.01

const val TEST_STEP = 0.02
const val ASSERT_DELTA = 0.001

class EpidemicGroupTest {

    private val epidemicGroup = EpidemicGroup(
        1998,
        TEST_START_HEALTHY,
        TEST_START_SICK,
        TEST_START_IMMUNE,
        TEST_START_VACCINATED,
        TEST_START_DEAD,
        0.3,
        3.5,
        0.5
    )

    @Test
    fun testProgressing() {
//        epidemicGroup.progress(
//            TEST_SICKEN,
//            TEST_REHAB,
//            TEST_VACCINATE,
//            TEST_DEATH,
//            TEST_STEP
//        )
//        showEpiGroup()
//        checkConsistency()
    }

    @Test
    fun testPropertiesAreInPlace() {
        assertProperties(
            TEST_START_HEALTHY,
            TEST_START_SICK,
            TEST_START_IMMUNE,
            TEST_START_VACCINATED,
            TEST_START_DEAD
        )
    }

    private fun checkConsistency() {
        val healthy = epidemicGroup.healthy
        val sick = epidemicGroup.sick
        val immune = epidemicGroup.immune
        val vaccinated = epidemicGroup.vaccinated
        val dead = epidemicGroup.dead
        assertEquals(1000.0, healthy + sick + immune + vaccinated + dead, ASSERT_DELTA)
    }

    private fun assertProperties(
        expectedHealthy: Double,
        expectedSick: Double,
        expectedImmune: Double,
        expectedVaccinated: Double,
        expectedDead: Double
    ) {
        val healthy = epidemicGroup.healthy
        val sick = epidemicGroup.sick
        val immune = epidemicGroup.immune
        val vaccinated = epidemicGroup.vaccinated
        val dead = epidemicGroup.dead
        assertEquals(expectedHealthy, healthy, ASSERT_DELTA)
        assertEquals(expectedSick, sick, ASSERT_DELTA)
        assertEquals(expectedImmune, immune, ASSERT_DELTA)
        assertEquals(expectedVaccinated, vaccinated, ASSERT_DELTA)
        assertEquals(expectedDead, dead, ASSERT_DELTA)
    }

    private fun showEpiGroup() {
        println("Healthy: " + epidemicGroup.healthy)
        println("Sick: " + epidemicGroup.sick)
        println("Immune: " + epidemicGroup.immune)
        println("Vaccinated: " + epidemicGroup.vaccinated)
        println("Dead: " + epidemicGroup.dead)
    }
}