package com.diplom.statistics;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.*;

import static java.lang.Math.pow;

public class MapDiffTest {

    @Test
    public void test() throws Exception {
        Map<Integer, Double> map1 = new HashMap<>();
        map1.put(1, 1.0);
        map1.put(2, 2.0);
        map1.put(3, 3.0);
        map1.put(4, 4.0);
        map1.put(5, 5.0);
        map1.put(6, 6.0);
        Map<Integer, Double> map2 = new HashMap<>();
        map2.put(6, 1.0);
        map2.put(5, 2.0);
        map2.put(4, 3.0);
        map2.put(3, 4.0);
        map2.put(2, 5.0);
        map2.put(1, 6.0);

        System.out.println(getMapDiffSquared(map1, map2));
    }

    @NotNull
    private Double getMapDiffSquared(@NotNull Map<Integer, Double> map1, @NotNull Map<Integer, Double> map2) {
        if (map1.size() != map2.size()) {
            throw new IllegalArgumentException("Map sizes don't match!");
        }
        return map1.entrySet().parallelStream().mapToDouble(entry -> pow(entry.getValue() - map2.get(entry.getKey()), 2)).sum();
    }

    @Test
    public void testTest() throws Exception {
        List<Double> doubles = Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0);
        System.out.println(getMedian(doubles));
    }

    private double getMedian(List<Double> values) {
        List<Double> sortedValues = new ArrayList<>(values);
        sortedValues.sort(Double::compareTo);
        int index = (sortedValues.size() / 2);
        return sortedValues.get(index);
    }
}
