package com.diplom.statistics;

import org.junit.Test;

public class DataImporterTest {

    private final DataImporter dataImporter = new DataImporter(
            "C:\\Code\\epidemic\\src\\test\\resources\\inputData.json", 1998, 2018);

    @Test
    public void importData() {
        CountryGraph russiaGraph = dataImporter.getCountryGraphMapCopy().get("Russian Federation");

        for (Double count : russiaGraph.getMap().values()) {
            System.out.println(count);
        }
    }
}