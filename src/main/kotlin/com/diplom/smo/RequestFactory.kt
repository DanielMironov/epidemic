package com.diplom.smo

import java.util.*

/**
 * Created by Daniel Mironov on 20.05.2019
 */

const val DEVIATION = 20.0

class RequestFactory(
    private val averageProcessTimeSeconds: Int,
    private val processorWorkPerSecond: Double,
    requestsPerMinute: Int,
    private val averageWaitTimeInSeconds: Int
) {
    private val requestTimeGapInSeconds: Int = 60 / requestsPerMinute

    fun isTimeForRequest(currentTime: Int): Boolean {
        return (currentTime % requestTimeGapInSeconds == 0)
    }

    fun getRequest(): Request {
        return Request(getRequestFullWorkAmount(), getRequestMaxWaitTime())
    }

    private fun getRequestMaxWaitTime(): Int {
        return gauss(averageWaitTimeInSeconds.toDouble(), DEVIATION).toInt()
    }

    private fun getRequestFullWorkAmount(): Double {
        val processTime = gauss(averageProcessTimeSeconds.toDouble(), DEVIATION)
        return processTime * processorWorkPerSecond
    }

    private fun gauss(middle: Double, deviation: Double): Double {
        val random = Random()
//        val stdRandom = random.nextDouble()
//        return (stdRandom * 2 * deviation) + (middle - deviation)
        val standardGauss = random.nextGaussian()
        return (standardGauss * deviation) + middle

    }
}