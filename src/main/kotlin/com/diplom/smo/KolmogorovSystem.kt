package com.diplom.smo

import com.diplom.math.RungeSolver

/**
 * Created by Daniel Mironov on 21.05.2019
 */

class KolmogorovSystem(
    states: List<Double>,
    processorsCount: Int,
    maxQueueLength: Int,
    averageProcessTimeSeconds: Int,
    requestsPerMinute: Int,
    averageWaitTimeInSeconds: Int
) {
    private val incomeStream = requestsPerMinute.toDouble() / 60.0
    private val processStream = (1.0 / averageProcessTimeSeconds.toDouble()) * processorsCount
    private val leaveStream = 1.0 / averageWaitTimeInSeconds.toDouble()

    var states = states
        private set

    private val statesCount = maxQueueLength + processorsCount + 1

    fun nextSecond() {
        val solver = RungeSolver()
        var i = 0
        while (i < 10) {
            states = solver.solve(
                { input: List<Double> -> diffFunction(input) },
                states,
                0.1
            )
            states = states.map {
                if (it >= 1.0) 1.0 else it
            }
            states = states.map {
                if (it <= 0.0) 0.0 else it
            }
            i++
        }
    }

    private fun diffFunction(input: List<Double>): List<Double> {
        if (input.size != statesCount)
            throw IllegalArgumentException("Wrong size input")
        val result = mutableListOf<Double>()
        result.add(
            (processStream + leaveStream) * input[1] - incomeStream * input[1]
        )
        var i = 1
        while (i < statesCount - 1) {
            result.add(
                (processStream + leaveStream) * input[i + 1]
                        + incomeStream * input[i - 1]
                        - (incomeStream + processStream + leaveStream) * input[i]
            )
            i++
        }
        result.add(
            incomeStream * input[statesCount - 2] - (processStream + leaveStream) * input[statesCount - 1]
        )
        return result
    }
}