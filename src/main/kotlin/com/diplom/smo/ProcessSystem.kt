package com.diplom.smo

import java.util.*

/**
 * Created by Daniel Mironov on 20.05.2019
 */

class ProcessSystem(
    private val processorsCount: Int,
    private val maxQueueLength: Int,
    averageProcessTimeSeconds: Int,
    processorWorkPerSecond: Double,
    requestsPerMinute: Int,
    averageWaitTimeInSeconds: Int
) {
    private val factory = RequestFactory(
        averageProcessTimeSeconds,
        processorWorkPerSecond,
        requestsPerMinute,
        averageWaitTimeInSeconds
    )

    private var currentTime: Int = 0
    private val workPerSecond = processorsCount * processorWorkPerSecond
    private val processedRequests = mutableListOf<Request>()
    private val requestQueue: Queue<Request> = LinkedList()

    val stateGraph = mutableListOf<Double>()


    fun nextSecond() {
        getNewRequest()
        processRequests()
        checkQueueForLeftRequests()

        stateGraph.add(processedRequests.size.toDouble() + requestQueue.size.toDouble())
        currentTime++
    }

    private fun getNewRequest() {
        if (factory.isTimeForRequest(currentTime)) {
            if (!queueIsFull()) {
                val request = factory.getRequest()
                if (allProcessorsAreBusy()) {
                    requestQueue.add(request)
                } else {
                    processedRequests.add(request)
                }
            }
        }
    }

    private fun processRequests() {
        val finishedRequests = mutableListOf<Request>()
        val nextRequests = mutableListOf<Request>()
        val workPerSecondPerRequest = workPerSecond / (processedRequests.size.toDouble())
        for (request in processedRequests) {
            request.process(workPerSecondPerRequest)
            if (request.isFinished()) {
                finishedRequests.add(request)
                if (!requestQueue.isEmpty()) {
                    val nextRequest = requestQueue.poll()
                    nextRequests.add(nextRequest)
                }
            }
        }
        processedRequests.removeAll(finishedRequests)
        processedRequests.addAll(nextRequests)
    }

    private fun checkQueueForLeftRequests() {
        val leftRequests = mutableListOf<Request>()
        for (request in requestQueue) {
            request.waitInQueue()
            if (request.isTimeToLeave()) {
                leftRequests.add(request)
            }
        }
        requestQueue.removeAll(leftRequests)
    }

    private fun queueIsFull(): Boolean {
        return requestQueue.size >= maxQueueLength
    }

    private fun allProcessorsAreBusy(): Boolean {
        return processedRequests.size >= processorsCount
    }

}