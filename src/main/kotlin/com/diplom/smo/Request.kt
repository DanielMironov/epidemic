package com.diplom.smo

/**
 * Created by Daniel Mironov on 20.05.2019
 */

class Request(private val fullWorkAmount: Double, private val maxWaitTime: Int) {
    private var doneWorkAmount: Double = 0.0
    private var actualWaitTime = 0

    fun isFinished(): Boolean {
        return doneWorkAmount >= fullWorkAmount
    }

    fun isTimeToLeave(): Boolean {
        return actualWaitTime >= maxWaitTime
    }

    fun waitInQueue() {
        actualWaitTime++
    }

    fun process(workAmount: Double) {
        doneWorkAmount += workAmount
    }
}