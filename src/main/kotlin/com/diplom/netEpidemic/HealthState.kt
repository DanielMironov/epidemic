package com.diplom.netEpidemic

enum class HealthState {
    HEALTHY,
    SICK,
    IMMUNE
}
