package com.diplom.netEpidemic

import java.lang.IllegalStateException

/**
 * Created by Daniel Mironov on 30.04.2019
 */

class NetEpidemicModel {
    var stats: Map<HealthState, List<Int>> = mapOf(
        Pair(HealthState.HEALTHY, emptyList()),
        Pair(HealthState.SICK, emptyList()),
        Pair(HealthState.IMMUNE, emptyList())
    )
        private set
    var people: Set<Person> = emptySet()
        private set

    fun getStatsByHealthState(healthState: HealthState): List<Int> {
        return stats.getOrElse(healthState, { throw IllegalStateException("No stats for $healthState!") })
    }

    fun add(people: Set<Person>) {
        val newPeople = people.toMutableList()
        newPeople.addAll(people)
        this.people = newPeople.toSet()
    }

    fun createNet(halfConnectionAmount: Int) {
        for (person in people) {
            person.connectAll(getAFewRandomPeople(halfConnectionAmount))
        }
    }

    private fun Person.connectAll(connections: List<Person>) {
        for (person in connections) {
            this.addConnection(person)
        }
    }

    private fun getAFewRandomPeople(amount: Int): List<Person> {
        return people.shuffled().take(amount)
    }

    fun getHealthyCount(): Int {
        return countPeopleByHealthState(HealthState.HEALTHY)
    }

    fun getSickCount(): Int {
        return countPeopleByHealthState(HealthState.SICK)
    }

    fun getImmuneCount(): Int {
        return countPeopleByHealthState(HealthState.IMMUNE)
    }

    private fun countPeopleByHealthState(healthState: HealthState): Int {
        return people.filter { it.healthState == healthState }.size
    }

    fun progress() {
        addToStats(HealthState.HEALTHY)
        addToStats(HealthState.SICK)
        addToStats(HealthState.IMMUNE)
    }

    private fun addToStats(healthState: HealthState) {
        val newStats = stats.toMutableMap()
        newStats[healthState] = getStatsByHealthState(healthState)
            .toMutableList().plus(countPeopleByHealthState(healthState)).toList()
        stats = newStats.toMap()
    }
}