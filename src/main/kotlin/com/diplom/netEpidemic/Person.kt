package com.diplom.netEpidemic

import com.diplom.netEpidemic.HealthState.*

class Person {
    var healthState: HealthState = HEALTHY
        private set
    var connections: Set<Person> = emptySet()
        private set

    fun addConnection(person: Person) {
        if (connections.contains(person))
            return
        val newConnections = connections.toMutableSet()
        newConnections.add(person)
        connections = newConnections.toSet()
        person.addConnection(this)
    }

    fun getSick() {
        healthState = SICK
    }

    fun getWell() {
        if (healthState == HEALTHY)
            throw IllegalStateException("Healthy person can't get immune!")
        healthState = IMMUNE
    }


}
