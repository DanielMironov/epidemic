package com.diplom.netEpidemic

/**
 * Created by Daniel Mironov on 28.04.2019
 */

const val GET_SICK_PROBABILITY = 0.1
const val SICK_DURATION = 20