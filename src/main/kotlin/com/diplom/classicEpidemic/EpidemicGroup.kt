package com.diplom.classicEpidemic

import com.diplom.math.RungeSolver
import java.lang.IllegalArgumentException
import kotlin.math.max

/**
 * Created by Daniel Mironov on 17.05.2019
 */

class EpidemicGroup(
    startYear: Int,
    healthy: Double,
    sick: Double,
    immune: Double,
    vaccinated: Double,
    dead: Double,
    newborn: Double,
    childrenVaccinated: Double,
    childrenNotVaccinated: Double
) {
    var year: Int = startYear
        private set
    var daysSinceNewYear: Int = 0
        private set
    var healthy: Double = healthy
        private set
    var sick: Double = sick
        private set
    var immune: Double = immune
        private set
    var vaccinated: Double = vaccinated
        private set
    var dead: Double = dead
        private set
    var newborn = newborn
        private set
    var childrenVaccinated = childrenVaccinated
        private set
    var childrenNotVaccinated = childrenNotVaccinated
        private set
    var sickNaturalDead = 0.0
        private set
    var immuneNaturalDead = 0.0
        private set
    private val caseMapPerYear: MutableMap<Int, Double> = mutableMapOf(Pair(startYear, 0.0))
    private val sickList: MutableList<Double> = mutableListOf()
    private val healthyList: MutableList<Double> = mutableListOf()
    private val immuneList: MutableList<Double> = mutableListOf()
    private val vaccinatedList: MutableList<Double> = mutableListOf()
    private val newbornList: MutableList<Double> = mutableListOf()
    private val childrenVaccinatedList: MutableList<Double> = mutableListOf()
    private val childrenNotVaccinatedList: MutableList<Double> = mutableListOf()

    fun getSumPeople(): Double {
        return healthy + sick + immune + vaccinated + dead + newborn + childrenNotVaccinated + childrenVaccinated
    }

    fun progress(
        function: (List<Double>) -> List<Double>,
        step: Double
    ) {
        sickList.add(sick)
        healthyList.add(healthy)
        immuneList.add(immune)
        vaccinatedList.add(vaccinated)
        newbornList.add(newborn)
        childrenVaccinatedList.add(childrenVaccinated)
        childrenNotVaccinatedList.add(childrenNotVaccinated)

        val solver = RungeSolver()

        val newProperties = solver.solve(
            function,
            listOf(
                healthy,
                sick,
                immune,
                vaccinated,
                dead,
                newborn,
                childrenVaccinated,
                childrenNotVaccinated,
                sickNaturalDead,
                immuneNaturalDead
            ),
            step
        )
        if (newProperties.size != 10)
            throw IllegalArgumentException("Got invalid input list!")
        healthy = if (newProperties[0] >= 0) newProperties[0] else 0.0
        val oldSick = sick
        val oldImmune = immune
        val oldSickDead = sickNaturalDead
        val oldDead = dead
        val oldImmuneDead = immuneNaturalDead
        sick = if (newProperties[1] >= 0) newProperties[1] else 0.0
        immune = if (newProperties[2] >= 0) newProperties[2] else 0.0
        vaccinated = if (newProperties[3] >= 0) newProperties[3] else 0.0
        dead = if (newProperties[4] >= 0) newProperties[4] else 0.0
        newborn = if (newProperties[5] >= 0) newProperties[5] else 0.0
        childrenVaccinated = if (newProperties[6] >= 0) newProperties[6] else 0.0
        childrenNotVaccinated = if (newProperties[7] >= 0) newProperties[7] else 0.0
        sickNaturalDead = if (newProperties[8] >= 0) newProperties[8] else 0.0
        immuneNaturalDead = if (newProperties[9] >= 0) newProperties[9] else 0.0
        val deltaSick = sick - oldSick
        val deltaImmune = immune - oldImmune
        val deltaDead = dead - oldDead
        val deltaSickDead = sickNaturalDead - oldSickDead
        val deltaImmuneDead = immuneNaturalDead - oldImmuneDead
        var newCases = deltaSick + deltaImmune + deltaImmuneDead + deltaDead + deltaSickDead
        if (newCases < 0) newCases = 0.0 //throw IllegalStateException("New cases less than zero")
        if (caseMapPerYear[year] == null) caseMapPerYear[year] = 0.0
        caseMapPerYear[year] = caseMapPerYear[year]!! + (newCases)
        daysSinceNewYear++
        if (daysSinceNewYear > 365) {
            year++
            daysSinceNewYear = 0
        }
    }

    public fun getImmutableCasePerYearMap(): Map<Int, Double> {
        return caseMapPerYear.toMap()
    }

    public fun getImmutableSickGraph(): List<Double> {
        return sickList.toList()
    }

    public fun getImmutableHealthyGraph(): List<Double> {
        return healthyList.toList()
    }

    public fun getImmutableImmuneGraph(): List<Double> {
        return immuneList.toList()
    }

    public fun getImmutableVaccinatedGraph(): List<Double> {
        return vaccinatedList.toList()
    }

    public fun getImmutableNewBornGraph(): List<Double> {
        return newbornList.toList()
    }

    public fun getImmutableChildrenNotVaccinatedGraph(): List<Double> {
        return childrenNotVaccinatedList.toList()
    }

    public fun getImmutableChildrenVaccinatedGraph(): List<Double> {
        return childrenVaccinatedList.toList()
    }

    fun diffEquation(
        childrenSicken: Double,
        vaccinatedChildrenSicken: Double,
        sicken: Double,
        vaccinatedSicken: Double,
        rehab: Double,
        newBornVaccinate: Double,
        newBornAvoidVaccination: Double,
        deathFromSicknessRate: Double,
        naturalDeathRate: Double,
        birthRate: Double,
        growRate: Double,
        input: List<Double>
    ): List<Double> {
        if (input.size != 10)
            throw IllegalArgumentException("Got invalid input list!")

        val healthy = input[0]
        val sick = input[1]
        val immune = input[2]
        val vaccinated = input[3]
        val newBorn = input[5]
        val childrenVaccinated = input[6]
        val childrenNotVaccinated = input[7]

        val healthyToSick = sicken * healthy * sick
        val vaccinatedToSick = vaccinatedSicken * vaccinated * sick
        val childrenToSick = childrenSicken * childrenNotVaccinated * sick
        val vaccinatedChildrenToSick = vaccinatedChildrenSicken * childrenVaccinated * sick
        val childrenGrown = childrenNotVaccinated * growRate
        val vaccinatedChildrenGrown = childrenVaccinated * growRate
        val sickToImmune = rehab * sick
        val sickToDead = deathFromSicknessRate * sick
        val born = birthRate * (healthy + immune + vaccinated + sick)
        val newBornVaccinated = newBornVaccinate * newBorn
        val newBornAvoidedVaccination = newBornAvoidVaccination * newBorn
        val healthyDied = healthy * naturalDeathRate
        val vaccinatedDied = vaccinated * naturalDeathRate
        val sickDied = sick * naturalDeathRate
        val immuneDied = immune * naturalDeathRate

        //output diff eq in the same order as input
        return listOf(
            childrenGrown - healthyToSick - healthyDied,
            healthyToSick + vaccinatedToSick + childrenToSick + vaccinatedChildrenToSick - sickToImmune - sickToDead - sickDied,
            sickToImmune - immuneDied,
            vaccinatedChildrenGrown - vaccinatedToSick - vaccinatedDied,
            sickToDead,
            born - newBornVaccinated - newBornAvoidedVaccination,
            newBornVaccinated - vaccinatedChildrenGrown - vaccinatedChildrenToSick,
            newBornAvoidedVaccination - childrenGrown - childrenToSick,
            sickDied,
            immuneDied
        )
    }
}
