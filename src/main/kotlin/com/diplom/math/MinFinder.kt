package com.diplom.math

import kotlin.math.abs
import kotlin.math.sqrt

class MinFinder {

    private val goldenSection = (1 + sqrt(5.0)) / 2

    fun findPoint(
        lesserInput: Double, biggerInput: Double, function: (Double) -> Double,
        previousPoint: Double?, previousPointFun: Double?
    ): Double {
        var lesser = lesserInput
        var bigger = biggerInput
        if (abs(bigger - lesser) < stoppingDelta * abs(bigger)) {
            println("Finder finished! Stopping difference:" + abs(bigger - lesser))
            println("Lesser:$lesser")
            println("Bigger:$bigger")
            println("Function in return: $previousPointFun")
            //System.out.println("Minimized function:" + function.run((bigger + lesser) / 2));
            return (bigger + lesser) / 2
        }
        if (lesser > bigger) {
            val t = lesser
            lesser = bigger
            bigger = t
        }
        //System.out.println("Finder working!");
        val lesserNew: Double = bigger - (bigger - lesser) / goldenSection
        val biggerNew: Double = lesser + (bigger - lesser) / goldenSection
        val funLesser: Double
        val funBigger: Double
        if (previousPoint == null) {
            funLesser = function(lesserNew)
            funBigger = function(biggerNew)
        } else {
            funLesser = if (abs(lesserNew - previousPoint) < 0.0001 * previousPoint) {
                previousPointFun!!
                //System.out.println("Lesser optimized!");
            } else function(lesserNew)
            funBigger = if (abs(biggerNew - previousPoint) < 0.0001 * previousPoint) {
                previousPointFun!!
                //System.out.println("Bigger optimized!");
            } else function(biggerNew)
        }
//        println("Lesser:$lesserNew")
//        println("Function in lesser:$funLesser")
//        println("Bigger:$biggerNew")
//        println("Function in bigger:$funBigger")
        if (funBigger == 0.0) return biggerNew
        if (funLesser == 0.0) return lesserNew
        return if (funLesser >= funBigger) {
            findPoint(lesserNew, bigger, function, biggerNew, funBigger)
        } else findPoint(lesser, biggerNew, function, lesserNew, funLesser)
    }

}

