package com.diplom.math

import kotlin.math.abs

class GradientCalculator {

    fun calculate(function: (List<Double>) -> Double, args: List<Double>): List<Double> {

        val result = mutableListOf<Double>()

        for (it in args.indices) {
            val arg = args[it]
            val delta = getDelta(arg)
            val newArgs = argListByIndex(arg + delta, args, it)
            val f1 = function.invoke(newArgs)
            val f2 = function.invoke(args)
            result.add((f1 - f2) / delta)
        }

        return result
    }

    private fun getDelta(arg: Double): Double {
        if (arg == 0.0) {
            return 0.0000000001
        }
        return abs(arg) * 0.000000001
    }
}