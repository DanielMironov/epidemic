package com.diplom.math


/**
 * Created by Daniel Mironov on 16.05.2019
 */

class RungeSolver {

    fun solve(function: (List<Double>)->List<Double>, currentValues: List<Double>, step: Double): List<Double> {

        val argument = ArrayList<Double>()
        val result = ArrayList<Double>()
        val k1 = function.invoke(currentValues)
        for (i in currentValues.indices) {
            argument.add(currentValues[i] + step / 2 * k1[i])
        }
        val k2 = function.invoke(argument)
        argument.clear()
        for (i in currentValues.indices) {
            argument.add(currentValues[i] + step / 2 * k2[i])
        }
        val k3 = function.invoke(argument)
        argument.clear()
        for (i in currentValues.indices) {
            argument.add(currentValues[i] + step * k3[i])
        }
        val k4 = function.invoke(argument)
        argument.clear()
        for (i in currentValues.indices) {
            result.add(currentValues[i] + step / 6 * (k1[i] + 2 * k2[i] + 2 * k3[i] + k4[i]))
        }
        return result
    }
}