package com.diplom.math

class MultiCoordinationMinFinder(
    private var points: List<Double>,
    private val bounds: List<Pair<Double, Double>>,
    private val function: (List<Double>) -> Double,
    private var delta: Double
) {
    init {
        if (delta == 0.0) delta = listAbs(points) * stoppingDelta
    }

    fun findPoints(): List<Double> {
        var exitFlag = false
        while (!exitFlag) {
            val oldPoints = points.toList()

            val gradientCalculator = GradientCalculator()
            val antiGradient = multiplyList(
                gradientCalculator.calculate(function, points),
                -1.0
            )
            val minFinder = MinFinder()

            val stepMultiplier = minFinder.findPoint(
                0.0,
                1000.0,
                oneDimensionalFunctionByVector(function, antiGradient),
                null,
                null
            )

            points = sumLists(oldPoints, multiplyList(antiGradient, stepMultiplier))
            points = enforceListInBounds(bounds, points)

            if (listDiff(oldPoints, points) < delta) {
                exitFlag = true
                println("MultiCoordinationMinFinder stopping!")
                println("Final diff: " + listDiff(oldPoints, points))
                println("Final abs: " + listAbs(oldPoints))
            } else {
                println("MultiCoordinationMinFinder continuing!")
                println("Current diff: " + listDiff(oldPoints, points))
                println("Points abs: " + listAbs(oldPoints))
            }
        }
        
        return points
    }

    private fun oneDimensionalFunctionByVector(function: (List<Double>) -> Double, vector: List<Double>): (Double) -> Double {
        if (vector.size != points.size) {
            throw IllegalArgumentException("Wrong dimension vector!")
        }
        return {
            function.invoke(
                sumLists(
                    points,
                    multiplyList(vector, it)
                )
            )
        }
    }

    private fun oneDimensionalFunctionByIndex(function: (List<Double>) -> Double, index: Int): (Double) -> Double {
        return {
            function.invoke(argListByIndex(
                it, points, index
            ))
        }
    }
}