package com.diplom.math

import java.lang.IllegalArgumentException
import kotlin.math.pow

const val peopleCountDivideFactor = 100.0

const val stoppingDelta = 0.0000001

fun argListByIndex(value: Double, list: List<Double>, index: Int): List<Double> {
    val resultList = list.toMutableList()
    resultList[index] = value

    return resultList.toList()
}

fun multiplyList(list: List<Double>, value: Double): List<Double> {
    return list.map {
        it * value
    }
}

fun sumLists(list1: List<Double>, list2: List<Double>): List<Double> {
    return list1.indices.map {
        list1[it] + list2[it]
    }
}

fun listDiff(list1: List<Double>, list2: List<Double>): Double {
    return list1.indices.map {
        (list1[it] - list2[it]).pow(2)
    }.sum().sqrt()
}

fun listAbs(list: List<Double>): Double {
    return list.map {
        it.pow(2)
    }.sum().sqrt()
}

fun Double.sqrt(): Double {
    return kotlin.math.sqrt(this)
}

fun enforceListInBounds(bounds: List<Pair<Double, Double>>, list: List<Double>): List<Double> {
    if (bounds.size != list.size) throw IllegalArgumentException("Dimensions dont match!")
    return list.indices.map {
        enforceDoubleInBound(bounds[it], list[it])
    }
}

fun enforceDoubleInBound(bound: Pair<Double, Double>, double: Double): Double {
    var result = double
    if (bound.first > bound.second) throw IllegalArgumentException("Wrong pair!")
    if (result < bound.first) result = bound.first
    if (result > bound.second) result = bound.second
    return result
}