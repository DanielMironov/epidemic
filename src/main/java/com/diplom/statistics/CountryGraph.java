package com.diplom.statistics;

import java.util.HashMap;
import java.util.Map;

import static com.diplom.math.CommonConstantsAndFunctionsKt.peopleCountDivideFactor;

public class CountryGraph {
    private final Map<Integer, Integer> casesByYearMap = new HashMap<>();
    private final Integer startYear;
    private final Integer endYear;

    public CountryGraph(Integer startYear, Integer endYear) {
        this.startYear = startYear;
        this.endYear = endYear;
    }

    public void addCaseCount(Integer year, Integer caseCount) {
        casesByYearMap.put(year, caseCount);
    }

    public Map<Integer, Double> getMap() {
        Map<Integer, Double> result = new HashMap<>();
        for (Map.Entry<Integer, Integer> entry : casesByYearMap.entrySet()) {
            if ((entry.getKey() >= startYear) && (entry.getKey() <= endYear)) {
                result.put(entry.getKey(), entry.getValue().doubleValue() / peopleCountDivideFactor);
            }
        }

        return result;
    }
}
