package com.diplom.statistics;

import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DataImporter {

    private final Map<String, CountryGraph> countryGraphMap = new HashMap<>();
    private final String inputFilePath;
    private final Integer startYear;
    private final Integer endYear;

    public DataImporter(String inputFilePath, Integer startYear, Integer endYear) {
        this.inputFilePath = inputFilePath;
        this.startYear = startYear;
        this.endYear = endYear;
        importData();
    }

    @SuppressWarnings("unchecked")
    private void importData() {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();


        try (FileReader reader = new FileReader(inputFilePath)) {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONObject root = (JSONObject) obj;

            JSONArray dataList = (JSONArray) root.get("fact");

            //Iterate over employee array
            dataList.forEach(data -> parseDataObject((JSONObject) data));

        } catch (FileNotFoundException e) {
            System.out.println("No such file!");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            System.out.println("Incorrect json input");
            e.printStackTrace();
        }
    }

    private void parseDataObject(@NotNull JSONObject data) {
        Integer value = Integer.valueOf((String) data.get("Value"));
        JSONObject dims = (JSONObject) data.get("dims");
        String country = (String) dims.get("COUNTRY");
        Integer year = Integer.valueOf((String) dims.get("YEAR"));

        CountryGraph countryGraph = countryGraphMap.get(country);

        if (countryGraph == null) {
            countryGraph = new CountryGraph(startYear, endYear);
            countryGraph.addCaseCount(year, value);
            countryGraphMap.put(country, countryGraph);
        } else {
            countryGraph.addCaseCount(year, value);
        }

    }

    public Map<String, CountryGraph> getCountryGraphMapCopy() {
        return new HashMap<>(countryGraphMap);
    }
}
