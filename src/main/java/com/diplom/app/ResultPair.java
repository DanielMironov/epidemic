package com.diplom.app;

import java.util.Comparator;

public class ResultPair {
    private final double point;
    private final double result;

    public ResultPair(double point, double result) {
        this.point = point;
        this.result = result;
    }

    public double getPoint() {
        return point;
    }

    public double getResult() {
        return result;
    }

    public static Comparator<ResultPair> getComparator() {
        return Comparator.comparingDouble(o -> o.result);
    }
}
