package com.diplom.app;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CoefficientsContainer {
    private final double startSick;
    private final double startChildren;
    private final double startNewBorn;
    private final double rehab;
    private final double newBornVaccinate;
    private final double deathFromSicknessRate;
    private final double naturalDeathRate;
    private final double birthRate;
    private final double growRate;
    private final double childrenSicken;
    private final double sicken;

    public CoefficientsContainer(double startSick,
                                 double startChildren,
                                 double startNewBorn,
                                 double rehab,
                                 double newBornVaccinate,
                                 double deathFromSicknessRate,
                                 double naturalDeathRate,
                                 double birthRate,
                                 double growRate,
                                 double childrenSicken,
                                 double sicken) {
        this.startSick = startSick;
        this.startChildren = startChildren;
        this.startNewBorn = startNewBorn;
        this.rehab = rehab;
        this.newBornVaccinate = newBornVaccinate;
        this.deathFromSicknessRate = deathFromSicknessRate;
        this.naturalDeathRate = naturalDeathRate;
        this.birthRate = birthRate;
        this.growRate = growRate;
        this.childrenSicken = childrenSicken;
        this.sicken = sicken;
    }

    public CoefficientsContainer(List<Double> coefficients) {
        if (coefficients.size() != 11) {
            throw new IllegalArgumentException("Wrong list for container!");
        }
        startSick = coefficients.get(0);
        startChildren = coefficients.get(1);
        startNewBorn = coefficients.get(2);
        rehab = coefficients.get(3);
        newBornVaccinate = coefficients.get(4);
        deathFromSicknessRate = coefficients.get(5);
        naturalDeathRate = coefficients.get(6);
        birthRate = coefficients.get(7);
        growRate = coefficients.get(8);
        childrenSicken = coefficients.get(9);
        sicken = coefficients.get(10);
    }

    public List<Double> getList() {
        return Stream.of(
                startSick,
                startChildren,
                startNewBorn,
                rehab,
                newBornVaccinate,
                deathFromSicknessRate,
                naturalDeathRate,
                birthRate,
                growRate,
                childrenSicken,
                sicken
        ).collect(Collectors.toList());
    }

    public double getStartSick() {
        return startSick;
    }

    public double getStartChildren() {
        return startChildren;
    }

    public double getStartNewBorn() {
        return startNewBorn;
    }

    public double getRehab() {
        return rehab;
    }

    public double getNewBornVaccinate() {
        return newBornVaccinate;
    }

    public double getDeathFromSicknessRate() {
        return deathFromSicknessRate;
    }

    public double getNaturalDeathRate() {
        return naturalDeathRate;
    }

    public double getBirthRate() {
        return birthRate;
    }

    public double getGrowRate() {
        return growRate;
    }

    public double getChildrenSicken() {
        return childrenSicken;
    }

    public double getSicken() {
        return sicken;
    }
}
