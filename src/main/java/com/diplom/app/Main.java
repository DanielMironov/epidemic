package com.diplom.app;

import com.diplom.classicEpidemic.EpidemicGroup;
import com.diplom.math.CommonConstantsAndFunctionsKt;
import com.diplom.math.MinFinder;
import com.diplom.math.MultiCoordinationMinFinder;
import com.diplom.math.RungeSolver;
import com.diplom.statistics.CountryGraph;
import com.diplom.statistics.DataImporter;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import kotlin.Pair;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.diplom.math.CommonConstantsAndFunctionsKt.*;
import static java.lang.Math.*;
import static javafx.scene.paint.Color.*;

/**
 * Created by Daniel Mironov on 15.05.2019
 */
public class Main extends Application {

    private static final double WIDTH = 1000;
    private static final double HEIGHT = 500;
    private static final double DS = 40;
    private static final double DP = 6;
    private static final Canvas canvas = new Canvas(WIDTH, HEIGHT);

    private final double lowMarginMultiplier = 0.9;
    private final double highMarginMultiplier = 1.1;

    private static final double year = 1.0;
    private static final double dayTime = year / 365.0;


    private static final Integer startYear = 1998;
    private static final Integer endYear = 2018;
    private static final Integer yearsToCut = 5;

    private double startSick = 10.0 / peopleCountDivideFactor;
    private double startChildren = 1352716.0 / peopleCountDivideFactor;//1352716.0 / peopleCountDivideFactor;
    private double startNewBorn = 303725.0 / peopleCountDivideFactor;//303725.0 / peopleCountDivideFactor;
    private final double allPeople = 146693000.0 / peopleCountDivideFactor; //

    private final double rehab = getCoefficientByDaysToAlmostZero(21);
    private final double newBornVaccinate = getCoefficientByDaysToAlmostZero(365) * 1.0;
    private final double newBornAvoidVaccination = 0.0; //getCoefficientByDaysToAlmostZero(365) * 0.0;
    private final double deathFromSicknessRate = getCoefficientByDaysToAddMultiplyByAmount(365, 0.00005);
    private final double naturalDeathRate = getCoefficientByDaysToAddMultiplyByAmount(365, 0.01122);//0.013685
    private final double birthRate = getCoefficientByDaysToAddMultiplyByAmount(365, 0.01122); //0.01122
    private final double growRate = getCoefficientByDaysToAlmostZero(365 * 4);
    private final double sicken = 0.000013;
    private final double childrenSicken = 0.05;


    @Override
    public void start(Stage stage) {
        stage.setScene(new Scene(new Group(canvas)));
        stage.show();
        show();
    }

    private void show() {

//        for (int i = 0; i < 5000; i++) {
//            group.progress(
//                    (input) -> group.diffEquation(
//                            sicken,
//                            vaccinatedSicken,
//                            rehab,
//                            newBornVaccinate,
//                            newBornAvoidVaccination,
//                            deathFromSicknessRate,
//                            naturalDeathRate,
//                            birthRate,
//                            input),
//                    dayTime
//            );
//            healthyGraph.add(group.getHealthy());
//            sickGraph.add(group.getSick());
//            immuneGraph.add(group.getImmune());
//            vaccinatedGraph.add(group.getVaccinated());
//            deadGraph.add(group.getDead());
//        }
        DataImporter dataImporter = new DataImporter("C:\\Code\\epidemic\\src\\test\\resources\\inputData.json",
                startYear, endYear);
        CountryGraph countryGraph;
        try {
            countryGraph = dataImporter.getCountryGraphMapCopy().get("Russian Federation");
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("Country with that name not found!");
        }

        double searchStep = 0.001;
        List<Double> searchSteps = getBetweenSearchPoints(searchStep);


//        double optimalChildrenSicken = searchSteps.parallelStream()
//                .map(it -> findOptimumBetween(it, it+searchStep, countryGraph.getMap()))
//                .map(point -> new ResultPair(
//                        point,
//                        sickenMinFunc(point,countryGraph.getMap())
//                ))
//                .sorted(ResultPair.getComparator())
//                .mapToDouble(ResultPair::getPoint)
//                .findFirst().orElseThrow(IllegalStateException::new);

//        double optimalChildrenSicken = findOptimumBetween(0.04, 0.05, countryGraph.getMap());
//
//        EpidemicGroup group = getFreshGroup(startChildren, startSick, startNewBorn);
//        runSimOnGroup(group, optimalChildrenSicken);

//        CoefficientsContainer optimalCoefficientsContainer =
//                findMultiDimensionalOptimum(
//                        getInitContainer(),
//                        getBounds(),
//                        countryGraph.getMap()
//                );

        EpidemicGroup group = runSimOnGroupListCoeff(getInitContainer());

        List<Double> statsGraph = getGraphFromMap(countryGraph.getMap());
        int size = statsGraph.size();
        int firstIndex = yearsToCut - 1;
        statsGraph = statsGraph.subList(firstIndex, size);
        double statsSum = statsGraph.stream().mapToDouble(it -> it).sum();
        System.out.println("Stats sum: " + statsSum);
        List<Double> simGraph = getGraphFromMap(group.getImmutableCasePerYearMap());
        simGraph = simGraph.subList(firstIndex, size);
        simGraph = multiplyList(simGraph, 0.025);
        double simSum = simGraph.stream().mapToDouble(it -> it).sum();
        System.out.println("Sim sum: " + simSum);
        if (simGraph.size() != statsGraph.size()) {
            throw new IllegalStateException("Graphs do not match sizes!");
        }
        List<Double> graph = Arrays.asList(15.8, 29.91, 25.44, 10.54, 11.47, 3.73, 1.27, 1.01, 1.29, 4.0, 14.53, 17.60, 30.11, 18.43, 11.78, 10.21, 19.39);
        System.out.println(statsGraph);
        System.out.println("Accuracy:");
        System.out.println(getMedianAccuracy(statsGraph, graph));
        List<List<Double>> rawData = Stream.of(graph, statsGraph) //.subList(firstIndex, lastIndex)
                .collect(Collectors.toList());
        formatAndDrawLists(rawData);

    }

    private double getFinalAccuracy(List<Double> stats, List<Double> sim) {
        if (stats.size() != sim.size()) {
            throw new IllegalArgumentException("Sizes dont match!");
        }
        double lastStat = stats.get(stats.size() - 1);
        double lastSim = sim.get(sim.size() - 1);

        return getAccuracy(lastStat, lastSim);
    }

    private double getAverageBinaryAccuracy(List<Double> stats, List<Double> sim) {
        if (stats.size() != sim.size()) {
            throw new IllegalArgumentException("Sizes dont match!");
        }
        List<Double> accuracyList = new ArrayList<>();
        for (int i = 1; i < stats.size(); i++) {
            accuracyList.add(getBinaryAccuracy(stats.get(i-1), stats.get(i), sim.get(i-1), sim.get(i)));
        }

        return accuracyList.stream().mapToDouble(it -> it).average().orElse(-1.0);
    }

    private double getBinaryAccuracy(double stat1, double stat2, double sim1, double sim2) {
        double statDelta = stat2 - stat1;
        double simDelta = sim2 - sim1;
        return (statDelta * simDelta > 0.0) ? 0.0 : 1.0;
    }

    private double getAverageDirectionalAccuracy(List<Double> stats, List<Double> sim) {
        if (stats.size() != sim.size()) {
            throw new IllegalArgumentException("Sizes dont match!");
        }
        List<Double> accuracyList = new ArrayList<>();
        for (int i = 1; i < stats.size(); i++) {
            accuracyList.add(getDirectionalAccuracy(stats.get(i-1), stats.get(i), sim.get(i-1), sim.get(i)));
        }

        return accuracyList.stream().mapToDouble(it -> it).average().orElse(-1.0);
    }

    private double getDirectionalAccuracy(double stat1, double stat2, double sim1, double sim2) {
        double statDelta = stat2 - stat1;
        double simDelta = sim2 - sim1;
        return abs(statDelta - simDelta) / abs(statDelta);
    }

    private double getAverageAccuracy(List<Double> stats, List<Double> sim) {
        if (stats.size() != sim.size()) {
            throw new IllegalArgumentException("Sizes dont match!");
        }
        List<Double> accuracyList = new ArrayList<>();
        for (int i = 0; i < stats.size(); i++) {
            accuracyList.add(getAccuracy(stats.get(i), sim.get(i)));
        }

        return accuracyList.stream().mapToDouble(it -> it).average().orElse(-1.0);
    }

    private double getMedianAccuracy(List<Double> stats, List<Double> sim) {
        if (stats.size() != sim.size()) {
            throw new IllegalArgumentException("Sizes dont match!");
        }
        List<Double> accuracyList = new ArrayList<>();
        for (int i = 0; i < stats.size(); i++) {
            accuracyList.add(getAccuracy(stats.get(i), sim.get(i)));
        }

        return getMedian(accuracyList);
    }

    private double getAccuracy(double stat, double sim) {
        return abs(sim - stat) / abs(stat);
    }

    private double getMedian(List<Double> values) {
        List<Double> sortedValues = new ArrayList<>(values);
        sortedValues.sort(Double::compareTo);
        int index = sortedValues.size() / 2;
        return sortedValues.get(index);
    }

    @NotNull
    private List<Double> testGraph() {
        List<Double> result = new ArrayList<>();

        double initialValue = 100000.0;
        int days = 365 * 20;
        double coefficient = getCoefficientByDaysToAddMultiplyByAmount(days, 0.25);
        List<Double> input = Stream.of(initialValue).collect(Collectors.toList());
        RungeSolver solver = new RungeSolver();
        int i = 0;
        while (i < days) {
            List<Double> newPoint = solver.solve(
                    (points) -> Stream.of(points.get(0) * coefficient).collect(Collectors.toList()),
                    input,
                    dayTime
            );
            result.add(newPoint.get(0));
            input = newPoint;
            i++;
        }


        return result;
    }

    private double getCoefficientByDaysToAlmostZero(int days) {
        return (2 * log(10)) / (dayTime * days);
    }

    private double getCoefficientByDaysToAddMultiplyByAmount(int days, double addMultiplyFactor) {
        return getCoefficientByDaysToMultiplyByAmount(days, 1.0 + addMultiplyFactor);
    }

    private double getCoefficientByDaysToMultiplyByAmount(int days, double multiplyFactor) {
        return (log(multiplyFactor)) / (dayTime * days);
    }

    @NotNull
    @Contract(value = " -> new", pure = true)
    private CoefficientsContainer getInitContainer() {
        return new CoefficientsContainer(
                startSick,
                startChildren,
                startNewBorn,
                rehab,
                newBornVaccinate,
                deathFromSicknessRate,
                naturalDeathRate,
                birthRate,
                growRate,
                childrenSicken,
                sicken
        );
    }

    @SuppressWarnings("DuplicatedCode")
    @NotNull
    private List<Pair<Double, Double>> getBounds() {
        List<Pair<Double, Double>> result = new ArrayList<>();

        result.add(new Pair<>(5.0 / peopleCountDivideFactor, 80000.0 / peopleCountDivideFactor));
        result.add(new Pair<>(startChildren * lowMarginMultiplier, startChildren * highMarginMultiplier));
        result.add(new Pair<>(startNewBorn * lowMarginMultiplier, startNewBorn * highMarginMultiplier));
        result.add(new Pair<>(rehab * lowMarginMultiplier, rehab * highMarginMultiplier));
        result.add(new Pair<>(newBornVaccinate * lowMarginMultiplier, newBornVaccinate * highMarginMultiplier));
        result.add(new Pair<>(deathFromSicknessRate * lowMarginMultiplier, deathFromSicknessRate * highMarginMultiplier));
        result.add(new Pair<>(naturalDeathRate * lowMarginMultiplier, naturalDeathRate * highMarginMultiplier));
        result.add(new Pair<>(birthRate * lowMarginMultiplier, birthRate * highMarginMultiplier));
        result.add(new Pair<>(growRate * lowMarginMultiplier, growRate * highMarginMultiplier));
        result.add(new Pair<>(0.0, 0.05));
        result.add(new Pair<>(0.0, 0.000001));

        return result;
    }

    @NotNull
    private CoefficientsContainer findMultiDimensionalOptimum(@NotNull CoefficientsContainer container,
                                                              List<Pair<Double, Double>> bounds,
                                                              Map<Integer, Double> casesPerYearStat
    ) {
        MultiCoordinationMinFinder finder = new MultiCoordinationMinFinder(
                container.getList(),
                bounds,
                (input) -> multiVarMinFunction(input, casesPerYearStat),
                stoppingDelta * 1000.0
        );

        List<Double> result = finder.findPoints();

        return new CoefficientsContainer(result);
    }

    @NotNull
    private Double multiVarMinFunction(List<Double> input,
                                       Map<Integer, Double> casesPerYearStat) {
        EpidemicGroup group = runSimOnGroupListCoeff(
                new CoefficientsContainer(input)
        );

        return getMapDiffSquared(casesPerYearStat, group.getImmutableCasePerYearMap());
    }

    @NotNull
    private EpidemicGroup runSimOnGroupListCoeff(
            @NotNull CoefficientsContainer container
    ) {
        final double vaccinatedChildrenSicken = container.getChildrenSicken() * 0.05;
        final double sicken = container.getSicken();
        final double vaccinatedSicken = sicken * 0.05;
        final double newBornAvoidVaccination = container.getNewBornVaccinate() * (0.1 / 0.9);

        EpidemicGroup preGroup = getFreshGroup(container.getStartChildren(), container.getStartSick(), container.getStartNewBorn());

        while (preGroup.getYear() <= endYear) {
            preGroup.progress(
                    (input) -> preGroup.diffEquation(
                            container.getChildrenSicken(),
                            vaccinatedChildrenSicken,
                            sicken,
                            vaccinatedSicken,
                            container.getRehab(),
                            container.getNewBornVaccinate(),
                            newBornAvoidVaccination,
                            container.getDeathFromSicknessRate(),
                            container.getNaturalDeathRate(),
                            container.getBirthRate(),
                            container.getGrowRate(),
                            input),
                    dayTime
            );
        }

        EpidemicGroup group = new EpidemicGroup(
                startYear,
                preGroup.getHealthy(),
                preGroup.getSick(),
                preGroup.getImmune(),
                preGroup.getVaccinated(),
                0.0,
                preGroup.getNewborn(),
                preGroup.getChildrenVaccinated(),
                preGroup.getChildrenNotVaccinated()
        );

        while (group.getYear() <= endYear) {
            group.progress(
                    (input) -> group.diffEquation(
                            container.getChildrenSicken(),
                            vaccinatedChildrenSicken,
                            sicken,
                            vaccinatedSicken,
                            container.getRehab(),
                            container.getNewBornVaccinate(),
                            newBornAvoidVaccination,
                            container.getDeathFromSicknessRate(),
                            container.getNaturalDeathRate(),
                            container.getBirthRate(),
                            container.getGrowRate(),
                            input),
                    dayTime
            );
        }

        return preGroup;
    }

    private double findOptimumBetween(double lesser, double bigger, Map<Integer, Double> stats) {
        MinFinder finder = new MinFinder();

        return finder.findPoint(
                lesser,
                bigger,
                sicken -> sickenMinFunc(sicken, stats),
                null,
                null
        );
    }

    @NotNull
    private List<Double> getBetweenSearchPoints(double step) {
        List<Double> result = new ArrayList<>();
        double currentPoint = 0.0;
        while (currentPoint < 1.0) {
            result.add(currentPoint);
            currentPoint += step;
        }
        return result;
    }

    @NotNull
    private Double sickenMinFunc(Double childrenSicken, Map<Integer, Double> casesPerYearStat) {
        EpidemicGroup group = getFreshGroup(startChildren, startSick, startNewBorn);
        runSimOnGroup(group, childrenSicken);

        return getMapDiffSquared(casesPerYearStat, group.getImmutableCasePerYearMap());

    }

    @NotNull
    private EpidemicGroup getFreshGroup(double startChildren, double startSick, double startNewBorn) {
        final double vaccinatedChildren = startChildren * 0.95;
        final double notVaccinatedChildren = startChildren - vaccinatedChildren;
        final double notSickAdults = allPeople - startSick - startChildren;
        final double vaccinatedAdults = notSickAdults * 0.95;
        final double healthy = (notSickAdults - vaccinatedAdults) * 0.95;
        final double immune = (notSickAdults - vaccinatedAdults) * 0.05;
        return new EpidemicGroup(
                startYear,
                healthy,
                startSick,
                immune,
                vaccinatedAdults,
                0.0,
                startNewBorn,
                vaccinatedChildren,
                notVaccinatedChildren
        );
    }

    private void runSimOnGroup(@NotNull EpidemicGroup group, double childrenSicken) {
        final double vaccinatedChildrenSicken = childrenSicken * 0.05;
        final double sicken = vaccinatedChildrenSicken * 0.00001;
        final double vaccinatedSicken = sicken * 0.05;
        final double newBornAvoidVaccination = newBornVaccinate * (0.1 / 0.9);

        while (group.getYear() <= endYear) {
            group.progress(
                    (input) -> group.diffEquation(
                            childrenSicken,
                            vaccinatedChildrenSicken,
                            sicken,
                            vaccinatedSicken,
                            rehab,
                            newBornVaccinate,
                            newBornAvoidVaccination,
                            deathFromSicknessRate,
                            naturalDeathRate,
                            birthRate,
                            growRate,
                            input),
                    dayTime
            );
        }
    }

    @NotNull
    private Double getMapDiffSquared(@NotNull Map<Integer, Double> map1, @NotNull Map<Integer, Double> map2) {
        if (map1.size() != map2.size()) {
            throw new IllegalArgumentException("Map sizes don't match!");
        }
        double result = map1.entrySet().stream().mapToDouble(entry -> abs(entry.getValue() - map2.get(entry.getKey()))).sum();
        if (result == Double.POSITIVE_INFINITY) {
            result = Double.MAX_VALUE;
        }
        return result;
    }


    private void formatAndDrawLists(List<List<Double>> rawData) {
        int stepsCount = rawData.stream().mapToInt(List::size).max().orElse(0);
        List<List<Double>> formattedLists = getFormattedPositiveLists(rawData);
        Iterator<Color> colorIterator = colorPool().iterator();
        for (List<Double> list : formattedLists) {
            draw(list, getNextColor(colorIterator), stepsCount);
        }
        //draw(formattedLists.get(formattedLists.size() - 1), GREY, stepsCount);
        drawVerticalGrid(stepsCount);
        drawHorizontalGrid(5);
    }

    @NotNull
    private List<Double> zeroList(int size) {
        List<Double> result = new ArrayList<>(size);

        for (int i = 0; i < size; i++) {
            result.add(0.0);
        }
        return result;
    }

    private List<Double> getGraphFromMap(@NotNull Map<Integer, Double> casesByYearMap) {
        List<Double> result = casesByYearMap.keySet().stream().sorted().parallel().filter(year -> year >= startYear).map(casesByYearMap::get).collect(Collectors.toList());
        return result;
    }

    @NotNull
    private List<List<Double>> reformMatrix(@NotNull List<List<Double>> matrix) {
        List<List<Double>> result = new ArrayList<>();
        if (matrix.isEmpty()) {
            return result;
        }
        int matrixLength = matrix.get(0).size();
        for (int i = 0; i < matrixLength; i++) {
            List<Double> newRow = new ArrayList<>();
            for (List<Double> list : matrix) {
                newRow.add(list.get(i));
            }
            result.add(newRow);
        }
        return result;
    }

    private Color getNextColor(@NotNull Iterator<Color> iterator) {
        if (!iterator.hasNext()) {
            iterator = colorPool().iterator();
        }
        return iterator.next();
    }

    private List<Color> colorPool() {
        return Stream.of(
                GREEN,
                RED,
                BLUE,
                CYAN,
                BROWN,
                VIOLET,
                DARKGOLDENROD,
                MAGENTA,
                GREY,
                GREENYELLOW,
                ORANGE,
                ORCHID,
                CORAL,
                DARKORANGE
        ).collect(Collectors.toList());
    }

    private List<Double> cosList(double coeff) {
        double k = 0;
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < 2000; i++) {
            result.add((Math.cos(k) + 1) * coeff);
            k += dayTime;
        }
        return result;
    }

    @Contract(pure = true)
    private List<List<Double>> getFormattedPositiveLists(@NotNull List<List<Double>> inputLists) {
        double max = inputLists.stream()
                .flatMap(Collection::stream).max(Double::compareTo).orElse(0.0);
        double min = inputLists.stream()
                .flatMap(Collection::stream).min(Double::compareTo).orElse(-10.0);
        if (min < 0)
            throw new IllegalArgumentException("Got negative list!");
        List<List<Double>> result = new ArrayList<>();
        double stretchCoeff = getStretchCoeffFromMax(max);
        for (List<Double> list : inputLists) {
            result.add(getMovedList(getStretchedList(list, stretchCoeff), 10));
        }
        return result;
    }

    @Contract(pure = true)
    private List<Double> getStretchedList(@NotNull List<Double> list, double stretchCoeff) {
        double moveAmount = getMoveAmountFromList(list);
        return list.stream()
                //.map(it -> it + moveAmount)
                .map(it -> it * stretchCoeff)
                //.map(it -> it - moveAmount)
                .collect(Collectors.toList());
    }

    @Contract(pure = true)
    private double getStretchCoeffFromList(@NotNull List<Double> list) {
        double max = list.stream().max(Double::compareTo).orElse(0.0);
        return getStretchCoeffFromMax(max);
    }

    @Contract(pure = true)
    private double getStretchCoeffFromMax(double max) {
        double border = (HEIGHT - 40);
        double minHeight = (HEIGHT / 2) + 10;
        return border / max;
//        if (max >= minHeight) {
//            if (max <= border) {
//                return 1.0;
//            } else {
//                return border / max;
//            }
//        } else {
//            return minHeight / max;
//        }
    }

    @Contract(pure = true)
    private List<Double> getMovedList(@NotNull List<Double> list, double moveAmount) {
        return list.stream().map(it -> it + moveAmount).collect(Collectors.toList());
    }

    @Contract(pure = true)
    private double getMoveAmountFromList(@NotNull List<Double> list) {
        double min = list.stream().min(Double::compareTo).orElse(0.0);
        return (double) 0 - min;
    }

    private void draw(@NotNull List<Double> listToDraw, Color color, int stepsCount) {
        double space = WIDTH / stepsCount;
        double k = 10;
        final GraphicsContext graphicsContext = canvas.getGraphicsContext2D();

//        graphicsContext.setFill(Color.WHITE);
//        graphicsContext.fillRect(0, 0, WIDTH, HEIGHT);

        double previousX = k;
        double previousY = HEIGHT - listToDraw.get(0);
        for (double number : listToDraw) {
            double x = k;
            double y = HEIGHT - number;
            graphicsContext.setFill(color);
            graphicsContext.setStroke(color);
            graphicsContext.strokeLine(previousX, previousY, x, y);
            //graphicsContext.fillOval(x, y, 2, 2);
            k += space;
            previousX = x;
            previousY = y;
        }
    }

    private void drawHorizontalGrid(int stepsCount) {
        double space = (HEIGHT - 30) / stepsCount;
        double k = 10;
        final GraphicsContext graphicsContext = canvas.getGraphicsContext2D();

        for (int i = 0; i <= stepsCount; i++) {
            graphicsContext.setFill(GREY);
            graphicsContext.setStroke(GREY);
            graphicsContext.strokeLine(0.0,HEIGHT - k, WIDTH,HEIGHT - k);
            k += space;
        }
    }

    private void drawVerticalGrid(int stepsCount) {
        double space = WIDTH / stepsCount;
        double k = 10;
        final GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        int half = stepsCount / 2;

        for (int i = 0; i < stepsCount; i++) {
            if (i == half) {
                graphicsContext.setFill(MAGENTA);
                graphicsContext.setStroke(MAGENTA);
            } else {
                graphicsContext.setFill(GREY);
                graphicsContext.setStroke(GREY);
            }
            graphicsContext.strokeLine(k, 0.0, k, HEIGHT);
            k += space;
        }
    }

    private int getMaxTenPow(double max) {
        int p = 0;
        while (true) {
            if (max < pow(10, p)) {
                return p - 1;
            } else {
                p++;
            }
        }
    }


    public static void main(String[] args) {

        launch(args);


    }
}
